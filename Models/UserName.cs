﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class UserName
    {
        [Key]
        public string Username { get; set; }
        public string UserID { get; set; }
        public string UserPrime { get; set; }
        public string UserPassWord { get; set; }
    }
}
