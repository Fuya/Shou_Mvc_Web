﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Prodcut",
                columns: table => new
                {
                    ProdcutName = table.Column<string>(nullable: true),
                    ProdcutID = table.Column<string>(nullable: false),
                    ProductImage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prodcut", x => x.ProdcutID);
                });

            migrationBuilder.CreateTable(
                name: "UserName",
                columns: table => new
                {
                    Username = table.Column<string>(nullable: false),
                    UserID = table.Column<string>(nullable: true),
                    UserPrime = table.Column<string>(nullable: true),
                    UserPassWord = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserName", x => x.Username);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Prodcut");

            migrationBuilder.DropTable(
                name: "UserName");
        }
    }
}
