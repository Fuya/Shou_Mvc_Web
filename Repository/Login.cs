﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Repository
{
    public class Login
    {
       public List<Models.UserName>  RequestId()        
       {
         using (var context = new Program.DdCreat())
         {
           return context.UserName.AsQueryable().ToList();
         }
       }
       
     //  public addUserNames() 

      public Boolean Create( Models.UserName item)
      {         
              if (item == null)
              {
                return false;
              }
              using (var context = new Program.DdCreat())
              {
                 context.UserName.Add(item);
                 context.SaveChanges();
                 return true;
              }
       }

      public Boolean Edit( Models.UserName item)
      {         
              if (item == null)
              {
                return false;
              }
              using (var context = new Program.DdCreat())
              {
                 context.UserName.Update(item);
                 context.SaveChanges();
                 return true;
              }
       }
      public Boolean Delete( Models.UserName item)
      {         
              if (item == null)
              {
                return false;
              }
              using (var context = new Program.DdCreat())
              {
                 context.UserName.Remove(item);
                 context.SaveChanges();
                 return true;
              }
       }
    }
}