using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace web.Controllers {
    public class LoginSuccess : Controller {

        public IActionResult index (string sucess) {
            var Users = new Repository.Login ().RequestId ().Select (x => new Models.UserName {
                UserID = x.UserID,
                    Username = x.Username,
                    UserPrime = x.UserPrime,
                    UserPassWord = x.UserPassWord
            });
            return View (Users);
        }

        [HttpPost]
        public IActionResult Create (Models.UserName item) {
            var Users = new Repository.Login ().Create (item);
            return Json (Url.Action ("Index", "LoginSuccess"));
            // return Json("登入失敗");   
        }

        [HttpPost]
        public IActionResult Edit (Models.UserName item) {
            var Users = new Repository.Login ().Edit (item);
            return Json (Url.Action ("Index", "LoginSuccess"));
            // return Json("登入失敗");   
        }

        [HttpPost]
        public IActionResult Delete (Models.UserName item) {
            var Users = new Repository.Login().Delete(item);
            return Json (Url.Action ("Index", "LoginSuccess"));
            // return Json("登入失敗");   
        }

    }
}